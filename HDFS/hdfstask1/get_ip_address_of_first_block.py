import subprocess
import sys
import re

if len(sys.argv) < 2:
    raise ValueError("Incorrect argv")

file_name = sys.argv[1]

info_about_all_blocks_process = subprocess.run(["hdfs", "fsck", file_name, "-files", "-blocks"], stdout=subprocess.PIPE,
                                               stderr=subprocess.PIPE)

info_about_all_blocks = info_about_all_blocks_process.stdout.decode()
block_id = re.search(r"blk_[^_]+", info_about_all_blocks).group(0)

info_about_block_process = subprocess.run(["hdfs", "fsck", "-blockId", block_id], stdout=subprocess.PIPE,
                                          stderr=subprocess.PIPE)

info_about_block = info_about_block_process.stdout.decode()
block_datanode = re.search(r"Block replica on datanode/rack: ([^/]+)/default is HEALTHY", info_about_block).group(1)

print(block_datanode)
