import os
import sys
import subprocess
import re
import getpass

devnull = open(os.devnull, 'wb+')


def main():
    if len(sys.argv) < 2:
        raise ValueError("Incorrect argv")

    new_file_size = int(sys.argv[1])
    my_user_name = "pd2020039"
    new_file_name = "zeros_{}.txt".format(my_user_name)

    subprocess.run(["dd", "if=/dev/zero", "of={}".format(new_file_name), "bs={}c".format(new_file_size),
                    "count=1"], stdout=devnull, stderr=devnull)

    subprocess.run(["hadoop", "fs", "-D dfs.replication=1", "-copyFromLocal", new_file_name], stdout=devnull,
                   stderr=devnull)

    print(new_file_size - find_file_real_size(new_file_name))

    subprocess.run(["rm", new_file_name], stdout=devnull, stderr=devnull)
    subprocess.run(["hadoop", "fs", "-rm", new_file_name], stdout=devnull, stderr=devnull)


def find_file_real_size(file_name):
    info_about_file_process = subprocess.run(
        ["hdfs", "fsck", "/user/{}/{}".format(getpass.getuser(), file_name), "-files", "-blocks", "-locations"],
        stdout=subprocess.PIPE, stderr=devnull)

    info_about_file = info_about_file_process.stdout.decode()
    parsed_file_info = re.findall(r"(BP-[0-9_.-]+):(blk_[0-9]+)[ a-zA-Z0-9=_]+\[DatanodeInfoWithStorage\[([^:]+)",
                                  info_about_file)

    summary_size = 0
    for block_info in parsed_file_info:
        block_pool, block_id, data_node_address = block_info
        summary_size += find_size_of_file(data_node_address, file_name_on_data_node(block_pool, block_id))

    return summary_size


def find_size_of_file(data_node_address, file_name):
    file_size_result = subprocess.run(
        ["sudo", "-u", "hdfsuser", "ssh", "hdfsuser@{}".format(data_node_address), "du -sb {}".format(file_name)],
        stdout=subprocess.PIPE, stderr=devnull).stdout.decode()
    size = int(re.search(r"(\d+)\t".format(file_name), file_size_result).group(1))
    return size


def file_name_on_data_node(block_pool, block_id):
    file_path = "/dfs/dn/current/{block_pool_name}/current/finalized/" \
                "subdir{first_subdirectory_id}/subdir{second_subdirectory_id}/{block_id}"

    block_id_number = int(block_id[4:])

    first_subdirectory_id = (block_id_number >> 16) & 0xff
    second_subdirectory_id = (block_id_number >> 8) & 0xff

    return file_path.format(block_pool_name=block_pool, block_id=block_id, first_subdirectory_id=first_subdirectory_id,
                            second_subdirectory_id=second_subdirectory_id)


if __name__ == '__main__':
    main()
