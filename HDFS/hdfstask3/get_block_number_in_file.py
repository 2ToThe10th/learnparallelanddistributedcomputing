import subprocess
import sys
import re

if len(sys.argv) < 2:
    raise ValueError("Incorrect argv")

file_name = sys.argv[1]

info_about_file_process = subprocess.run(["hdfs", "fsck", file_name], stdout=subprocess.PIPE,
                                         stderr=subprocess.PIPE)

info_about_file = info_about_file_process.stdout.decode()

block_number = re.search(r" Total blocks \(validated\):	(\d+) \(", info_about_file).group(1)

print(block_number)
