import subprocess
import sys
import re
import requests


def main():
    if len(sys.argv) < 2:
        raise ValueError("Incorrect argv")

    block_id = sys.argv[1]

    block_id_number = int(block_id[4:])

    first_subdirectory_id = (block_id_number >> 16) & 0xff
    second_subdirectory_id = (block_id_number >> 8) & 0xff

    file_path = "{block_datanode}:/dfs/dn/current/{block_pool_name}/current/finalized/" \
                "subdir{first_subdirectory_id}/subdir{second_subdirectory_id}/{block_id}"

    block_pool_name = get_block_pool_name()

    info_about_block = subprocess.check_output(["hdfs", "fsck", "-blockId", block_id], stderr=subprocess.DEVNULL).decode()

    block_datanode = re.search(r"Block replica on datanode/rack: ([^/]+)/default is HEALTHY", info_about_block).group(1)

    print(file_path.format(block_datanode=block_datanode, block_pool_name=block_pool_name,
                           first_subdirectory_id=first_subdirectory_id,
                           second_subdirectory_id=second_subdirectory_id, block_id=block_id))


def get_block_pool_name():
    name_node_info = requests.get("http://localhost:50070/jmx?qry=Hadoop:service=NameNode,name=NameNodeInfo").json()

    return name_node_info["beans"][0]["BlockPoolId"]


if __name__ == '__main__':
    main()
