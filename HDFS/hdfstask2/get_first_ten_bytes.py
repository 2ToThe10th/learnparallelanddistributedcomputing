import sys
import requests

if len(sys.argv) < 2:
    raise ValueError("Incorrect argv")

file_name = sys.argv[1]

kGetFilePath = "http://{host}:{http_port}/webhdfs/v1{file_path}?op=OPEN&length={length}"

kHost = "mipt-master.atp-fivt.org"
kHttpPort = 50070
kLength = 10

answer = requests.get(kGetFilePath.format(host=kHost, http_port=kHttpPort, file_path=file_name, length=kLength))

print(answer.text)
