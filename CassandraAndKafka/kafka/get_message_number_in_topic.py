import sys

from kafka import KafkaConsumer, TopicPartition


def main():
    if len(sys.argv) < 2:
        print("Write topic name as first argument")
        return

    topic_name = sys.argv[1]

    consumer = KafkaConsumer(topic_name,
                             bootstrap_servers=[
                                 'mipt-node04.atp-fivt.org:9092',
                                 'mipt-node05.atp-fivt.org:9092',
                                 'mipt-node06.atp-fivt.org:9092',
                                 'mipt-node07.atp-fivt.org:9092',
                                 'mipt-node08.atp-fivt.org:9092',
                             ])

    number_of_message = 0

    partitions = [TopicPartition(topic_name, partition) for partition in consumer.partitions_for_topic(topic_name)]

    for end_offset in consumer.end_offsets(partitions).values():
        number_of_message += end_offset

    for beginning_offset in consumer.beginning_offsets(partitions).values():
        number_of_message -= beginning_offset

    print(number_of_message)

    consumer.close()


if __name__ == '__main__':
    main()
