import argparse

from cassandra.cluster import Cluster

parser = argparse.ArgumentParser(
    description="Делает выборку фильмов, которые имеют тег tag, а user_id находится в интервале от start_id до end_id")

parser.add_argument('--tag', type=str, required=True)
parser.add_argument('--start_id', type=int, required=True, help="low user id including")
parser.add_argument('--end_id', type=int, required=True, help="top user id including")
parser.add_argument('--host', type=str, nargs='+', default=['localhost', ], help="cassandra host to connect")

args = parser.parse_args()
tag = args.tag
start_user_id = args.start_id
end_user_id = args.end_id


cluster = Cluster(args.host)
session = cluster.connect('test')

result = session.execute("""
SELECT * FROM test.videos_metadata_hob2020039 
WHERE tag = %s AND user_id >= %s AND user_id <= %s;
""", (tag, start_user_id, end_user_id))

print('\n'.join([row.title for row in result.all()]))

cluster.shutdown()
