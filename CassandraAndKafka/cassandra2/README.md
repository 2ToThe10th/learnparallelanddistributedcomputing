# PD Final task 3

## Usage:

run.sh [-h] --tag TAG --start_id START_ID --end_id END_ID [--host HOST [HOST ...]]

Делает выборку фильмов, которые имеют тег tag, а user_id находится в интервале от start_id до end_id

### optional arguments:

* -h, --help show this help message and exit
* --tag TAG
* --start_id START_ID low user id including
* --end_id END_ID top user id including
* --host HOST [HOST ...]    cassandra host to connect
