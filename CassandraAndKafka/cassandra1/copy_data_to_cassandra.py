import csv
import sys
from cassandra.cluster import Cluster


def main():
    if len(sys.argv) < 4:
        print("Not enough arguments")
        return

    csv_file_name = sys.argv[1]

    cluster = Cluster([sys.argv[2], ])

    session = cluster.connect()
    session.execute("""CREATE KEYSPACE test 
                        WITH replication = {'class': 'SimpleStrategy', 'replication_factor': 2} """)
    session.set_keyspace('test')

    table_name = sys.argv[3]

    session.execute(f"""CREATE TABLE "{table_name}" (
        tag text,
        added_year int,
        video_id int,
        added_date timestamp,
        description text,
        title text,
        user_id int,
        PRIMARY KEY (tag, user_id, title)
    )""")

    with open('pi') as pi_const:
        with open('e') as e_const:
            with open(csv_file_name, newline='') as csv_file:
                data = csv.reader(csv_file, delimiter=',', quotechar='"')

                index = 0
                is_header = True
                for row in data:
                    if is_header:
                        is_header = False
                        continue
                    if len(row) != 7:
                        print(f"Incorrect data: {row}")
                        continue
                    index += 1
                    row[1] = int(row[1])
                    row[2] = index * int(pi_const.read(1))
                    row[6] = index * int(e_const.read(1))
                    insert_data_to_cassandra(row, session, table_name)

    cluster.shutdown()


def insert_data_to_cassandra(data, session, table_name):
    try:
        session.execute(f"""
            INSERT INTO test."{table_name}" (tag, added_year, video_id, added_date, description, title, user_id) 
            VALUES (%s, %s, %s, %s, %s, %s, %s)
        """, data)
    except Exception as e:
        print(f"Exception in insert: {e}")
        print(data)
        exit(0)


if __name__ == '__main__':
    main()
