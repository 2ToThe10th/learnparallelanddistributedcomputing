#include <iostream>
#include <algorithm>
#include <random>
#include <memory>
#include "CPU/QuickSort.h"
#include "CPU/Timer.h"
#include "GPU/Timer.cuh"
#include "GPU/QuickSort.h"

int main(int argc, char **argv) {

  if (argc < 2) {
    std::cout << "Too less arguments" << std::endl;
    return 1;
  }

  unsigned array_size = strtoull(argv[1], nullptr, 10);

  if (array_size == 0) {
    std::cout << "Array size might be integer more than 0" << std::endl;
    return 2;
  }

  constexpr int kTEST = 1;

  for (int test = 0; test < kTEST; ++test) {

    auto array_std = std::make_unique<int[]>(array_size);
    auto array_our_cpu = std::make_unique<int[]>(array_size);
    auto array_our_gpu = std::make_unique<int[]>(array_size);

    std::mt19937 random(42);
    std::uniform_int_distribution<int> limits(-1e9 - 7, 1e9 + 7);

    for (int i = 0; i < array_size; ++i) {
      array_std[i] = limits(random);
      array_our_cpu[i] = array_std[i];
      array_our_gpu[i] = array_std[i];
    }

    std::cout << "GPU Sort Start" << std::endl;
    GPU::Timer gpu_quick_sort_timer_with_allocate_memory;
    gpu_quick_sort_timer_with_allocate_memory.Start();
    double time_work_on_device = GPU::QuickSort(array_our_gpu.get(), array_size);
    gpu_quick_sort_timer_with_allocate_memory.Finish();
    std::cout << "GPU Sort Done for " << gpu_quick_sort_timer_with_allocate_memory.GetIntervalTime()
              << " ----------------------------------" << std::endl;

    CPU::Timer cpu_quick_sort_timer, std_sort_timer;
    std::cout << "CPU Sort Start" << std::endl;
    cpu_quick_sort_timer.Start();
    CPU::QuickSort(array_our_cpu.get(), array_size);
    cpu_quick_sort_timer.Finish();
    std::cout << "CPU Sort Done for " << cpu_quick_sort_timer.GetIntervalTime()
              << " ----------------------------------" << std::endl;

    std::cout << "STD Sort Start" << std::endl;
    std_sort_timer.Start();
    std::sort(array_std.get(), array_std.get() + array_size);
    std_sort_timer.Finish();
    std::cout << "STD Sort Done for " << std_sort_timer.GetIntervalTime()
              << " ----------------------------------" << std::endl;

    for (int i = 0; i < array_size; ++i) {
      if (array_our_cpu[i] != array_std[i]) {
        std::cout << "[CPU] ERROR i = " << i << " array_our_cpu[i] = " << array_our_cpu[i] << "  array_std[i] = "
                  << array_std[i]
                  << std::endl;
      }
      if (array_our_gpu[i] != array_std[i]) {
        std::cout << "[GPU] ERROR i = " << i << " array_our_gpu[i] = " << array_our_gpu[i] << "  array_std[i] = "
                  << array_std[i]
                  << std::endl;
      }
    }

    std::cout << "GPU Quick Sort only computations: " << time_work_on_device << " ms\n"
              << "GPU Quick Sort with allocate memory: " << gpu_quick_sort_timer_with_allocate_memory.GetIntervalTime()
              << " ms\n"
              << "CPU Quick Sort: " << cpu_quick_sort_timer.GetIntervalTime() << " ms\n"
              << "STD Sort: " << std_sort_timer.GetIntervalTime() << " ms\n";

  }
}