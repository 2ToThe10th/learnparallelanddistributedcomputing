//
// Created by 2ToThe10th on 10.10.2020.
//

#ifndef CMAKE_SRC_TIMER_H_
#define CMAKE_SRC_TIMER_H_

class Timer {
 public:
  virtual void Start() = 0;
  virtual void Finish() = 0;
  virtual double GetIntervalTime() = 0;
};

#endif //CMAKE_SRC_TIMER_H_
