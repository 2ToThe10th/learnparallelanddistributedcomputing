//
// Created by 2ToThe10th on 08.10.2020.
//

#include <algorithm>
#include "QuickSort.h"

namespace CPU {

inline int FindPartition(const int array[], size_t size);
inline ssize_t SplitArrayByPartition(int array[], size_t size, int partition);
inline void RunQuickSortFromParts(int array[], size_t size, ssize_t split_pointer);

void QuickSort(int array[], size_t size) {

  if (size <= 2) {
    if (size == 2) {
      if (array[0] > array[1]) {
        std::swap(array[0], array[1]);
      }
    }
    return;
  }

  int partition = FindPartition(array, size);

  int split_pointer = SplitArrayByPartition(array, size, partition);

  RunQuickSortFromParts(array, size, split_pointer);
}

int FindPartition(const int array[], size_t size) {
  int sum_first_and_last = array[0] + array[size - 1];
  if (sum_first_and_last < 0) {
    --sum_first_and_last;
  }
  return sum_first_and_last / 2;
}

ssize_t SplitArrayByPartition(int *array, size_t size, int partition) {
  ssize_t first_pointer = -1;
  ssize_t last_pointer = size;

  while (first_pointer < last_pointer) {
    do {
      ++first_pointer;
    } while (first_pointer < last_pointer && array[first_pointer] <= partition);

    do {
      --last_pointer;
    } while (first_pointer < last_pointer && array[last_pointer] > partition);

    if (first_pointer < last_pointer) {
      std::swap(array[first_pointer], array[last_pointer]);
    }
  }

  if (array[last_pointer] > partition) {
    return last_pointer;
  } else {
    return last_pointer + 1;
  }
}

void RunQuickSortFromParts(int array[], size_t size, ssize_t split_pointer) {
  if (split_pointer < size) {
    QuickSort(array, split_pointer);
    QuickSort(array + split_pointer, size - split_pointer);
  } else { // last element - max element due to find partition
    QuickSort(array, size - 1);
  }
}

}