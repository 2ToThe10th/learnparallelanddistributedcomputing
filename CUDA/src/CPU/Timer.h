//
// Created by 2ToThe10th on 10.10.2020.
//

#ifndef CMAKE_SRC_CPU_TIMER_H_
#define CMAKE_SRC_CPU_TIMER_H_

#include <chrono>
#include "../Timer.h"

namespace CPU {

class Timer : public ::Timer {
 public:
  void Start() final {
    start = std::chrono::steady_clock::now();
  }
  void Finish() final {
    finish = std::chrono::steady_clock::now();
  }
  double GetIntervalTime() final {
    constexpr double kNanoToMilli = 1e6;
    std::chrono::nanoseconds interval = finish - start;
    return interval.count() / kNanoToMilli;
  }
 private:
  std::chrono::time_point<std::chrono::steady_clock> start;
  std::chrono::time_point<std::chrono::steady_clock> finish;
};

}

#endif //CMAKE_SRC_CPU_TIMER_H_
