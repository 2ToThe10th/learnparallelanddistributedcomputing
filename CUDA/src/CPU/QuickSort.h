//
// Created by 2ToThe10th on 08.10.2020.
//

#ifndef CMAKE_SRC_CPUQUICKSORT_H_
#define CMAKE_SRC_CPUQUICKSORT_H_

namespace CPU {

void QuickSort(int array[], size_t size);

}

#endif //CMAKE_SRC_CPUQUICKSORT_H_
