//
// Created by 2ToThe10th on 15.10.2020.
//

#include "ScanUtils.cuh"
#include "Scan.cuh"
#include "Constant.h"

namespace GPU {

__global__
void FillStartArrayLess(int *array, unsigned int size, int *array_to_fill, int partition);

__global__
void FillStartArrayLeq(int *array, unsigned int size, int *array_to_fill, int partition);

void ScanLess(int *array,
              unsigned int size,
              int *answer_array,
              int partition,
              int *helper_scan_array,
              cudaStream_t &stream) {
  unsigned number_of_blocks = (size + kMaxBlockSize - 1) / kMaxBlockSize;
  FillStartArrayLess<<<number_of_blocks, kMaxBlockSize, 0, stream>>>(array, size, answer_array, partition);
  Scan(answer_array, size, helper_scan_array, stream);
}

void ScanLeq(int *array,
             unsigned int size,
             int *answer_array,
             int partition,
             int *helper_scan_array,
             cudaStream_t &stream) {
  unsigned number_of_blocks = (size + kMaxBlockSize - 1) / kMaxBlockSize;
  FillStartArrayLeq<<<number_of_blocks, kMaxBlockSize, 0, stream>>>(array, size, answer_array, partition);
  Scan(answer_array, size, helper_scan_array, stream);
}

__global__
void FillStartArrayLess(int *array, unsigned int size, int *array_to_fill, int partition) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  if (index < size) {
    if (array[index] < partition) {
      array_to_fill[index] = 1;
    } else {
      array_to_fill[index] = 0;
    }
  }
}

__global__
void FillStartArrayLeq(int *array, unsigned int size, int *array_to_fill, int partition) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  if (index < size) {
    if (array[index] <= partition) {
      array_to_fill[index] = 1;
    } else {
      array_to_fill[index] = 0;
    }
  }
}
}