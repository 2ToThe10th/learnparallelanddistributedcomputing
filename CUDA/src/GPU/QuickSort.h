//
// Created by 2ToThe10th on 10.10.2020.
//

#ifndef CMAKE_SRC_GPUQUICKSORT_CUH_
#define CMAKE_SRC_GPUQUICKSORT_CUH_

#include <stdio.h>

namespace GPU {

/**
 * Sort array [array, array + size)
 *
 * @param array - start of the array
 * @param size - array size
 * @return time for device operations
 */
double QuickSort(int array[], unsigned int size);

}

#endif //CMAKE_SRC_GPUQUICKSORT_CUH_
