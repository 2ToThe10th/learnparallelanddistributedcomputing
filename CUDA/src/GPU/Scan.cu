//
// Created by 2ToThe10th on 16.10.2020.
//

#include <iostream>
#include "Scan.cuh"
#include "Constant.h"

#define NUM_BANKS 32
#define LOG_NUM_BANKS (unsigned)5

#define GET_SHIFT_INDEX(index) ((index) + ((index) >> LOG_NUM_BANKS))

namespace GPU {

__global__
void SegmentScanIncludingElem(int *array, unsigned int size);

__global__
void SegmentScanNoIncludingElem(int *in_array, unsigned int size, unsigned int frequency, int *out_array);

__device__
void SegmentScanMain(int *shared_array, unsigned int index, unsigned int block_size);

__global__
void Add(int *array, unsigned size, unsigned frequency, int *add_array);

void Scan(int *array, unsigned int size, int *helper_array, cudaStream_t &stream) {

  unsigned number_of_blocks = (((size + 1) / 2) + kMaxBlockSize - 1)
      / kMaxBlockSize;

  SegmentScanIncludingElem<<<number_of_blocks, kMaxBlockSize,
  GET_SHIFT_INDEX(2 * sizeof(int) * kMaxBlockSize), stream>>>(array, size);

  const unsigned frequency_for_one = 2 * kMaxBlockSize;
  long long current_frequency = frequency_for_one;

  while (current_frequency < size) {
    const unsigned out_array_size = (size + current_frequency - 1) / current_frequency;
    int *out_array = helper_array;

    number_of_blocks = (((out_array_size + 1) / 2) + kMaxBlockSize - 1) / kMaxBlockSize;

    SegmentScanNoIncludingElem<<<number_of_blocks,
    kMaxBlockSize,
    GET_SHIFT_INDEX(2 * sizeof(int) * kMaxBlockSize),
    stream>>>(array,
              size,
              current_frequency,
              out_array);

    Add<<<(size + kMaxBlockSize - 1) / kMaxBlockSize,
    kMaxBlockSize, 0, stream>>>(array,
                                size,
                                current_frequency,
                                out_array);

    current_frequency *= frequency_for_one;
  }
}

//<<<(((size + 1) / 2) + kMaxBlockSize - 1) / kMaxBlockSize , kMaxBlockSize, GET_SHIFT_INDEX(2 * sizeof(int) * kMaxBlockSize)>>>
__global__ void SegmentScanIncludingElem(int *array, unsigned int size) {
  const int block_size = blockDim.x;
  const int index = blockIdx.x * block_size + threadIdx.x;
  const int thread_index = threadIdx.x;

  extern __shared__ int shared_array[];

  if (2 * index < size) {
    shared_array[GET_SHIFT_INDEX(2 * thread_index)] = array[2 * index];
  } else {
    shared_array[GET_SHIFT_INDEX(2 * thread_index)] = 0;
  }

  if (2 * index + 1 < size) {
    shared_array[GET_SHIFT_INDEX(2 * thread_index + 1)] = array[2 * index + 1];
  } else {
    shared_array[GET_SHIFT_INDEX(2 * thread_index + 1)] = 0;
  }

  SegmentScanMain(shared_array, thread_index, block_size);

  if (2 * index < size) {
    array[2 * index] += shared_array[GET_SHIFT_INDEX(2 * thread_index)];
  }
  if (2 * index + 1 < size) {
    array[2 * index + 1] += shared_array[GET_SHIFT_INDEX(2 * thread_index + 1)];
  }
}

//<<<((([(size + frequency - 1) / frequency] + 1) / 2) + kMaxBlockSize - 1) / kMaxBlockSize , kMaxBlockSize, GET_SHIFT_INDEX(2 * sizeof(int) * kMaxBlockSize)>>>
__global__ void SegmentScanNoIncludingElem(int *in_array, unsigned int size, unsigned frequency, int *out_array) {
  const unsigned block_size = blockDim.x;
  const unsigned index = blockIdx.x * block_size + threadIdx.x;
  const unsigned thread_index = threadIdx.x;

  extern __shared__ int shared_array[];

  unsigned for_shared_array_index = (2 * index + 1) * frequency - 1;

  if (for_shared_array_index < size) {
    shared_array[GET_SHIFT_INDEX(2 * thread_index)] = in_array[for_shared_array_index];
  } else {
    shared_array[GET_SHIFT_INDEX(2 * thread_index)] = 0;
  }
  if (for_shared_array_index + frequency < size) {
    shared_array[GET_SHIFT_INDEX(2 * thread_index + 1)] = in_array[for_shared_array_index + frequency];
  } else {
    shared_array[GET_SHIFT_INDEX(2 * thread_index + 1)] = 0;
  }

  SegmentScanMain(shared_array, thread_index, block_size);

  if (2 * index < size) {
    out_array[2 * index] = shared_array[GET_SHIFT_INDEX(2 * thread_index)];
  }
  if (2 * index + 1 < size) {
    out_array[2 * index + 1] = shared_array[GET_SHIFT_INDEX(2 * thread_index + 1)];
  }
}

__device__
void SegmentScanMain(int *shared_array, unsigned int index, unsigned int block_size) {
  for (unsigned multiply = 1; multiply <= block_size; multiply <<= 1) {
    const unsigned add_to_elem_index = multiply * (2 * index + 2) - 1;
    if (add_to_elem_index < 2 * block_size) {
      const unsigned added_elem_index = add_to_elem_index - multiply;
      shared_array[GET_SHIFT_INDEX(add_to_elem_index)] += shared_array[GET_SHIFT_INDEX(added_elem_index)];
    }
    __syncthreads();
  }

  if (index == block_size - 1) {
    shared_array[GET_SHIFT_INDEX(2 * block_size - 1)] = 0;
  }

  __syncthreads();

  for (unsigned multiply = block_size; multiply >= 1; multiply >>= 1) {
    const unsigned second_index = 2 * index * multiply + 2 * multiply - 1;
    if (second_index < 2 * block_size) {
      const unsigned first_index = second_index - multiply;
      const int temp = shared_array[GET_SHIFT_INDEX(second_index)];
      shared_array[GET_SHIFT_INDEX(second_index)] += shared_array[GET_SHIFT_INDEX(first_index)];
      shared_array[GET_SHIFT_INDEX(first_index)] = temp;
    }

    __syncthreads();
  }
}

__global__ void Add(int *array, unsigned int size, unsigned int frequency, int *add_array) {
  const unsigned index = blockIdx.x * blockDim.x + threadIdx.x;
  if (index < size) {
    array[index] += add_array[index / frequency];
  }
}

}