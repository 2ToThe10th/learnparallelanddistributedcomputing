//
// Created by 2ToThe10th on 10.10.2020.
//

#ifndef CMAKE_SRC_GPU_TIMER_CUH_
#define CMAKE_SRC_GPU_TIMER_CUH_

#include <driver_types.h>
#include <cuda_runtime.h>
#include <cstdio>
#include "../Timer.h"

namespace GPU {
class Timer : public ::Timer {
 public:
  Timer() {
    cudaEventCreate(&start);
    cudaEventCreate(&finish);
  }

  void Start() final {
    cudaEventRecord(start);
    cudaEventSynchronize(start);
  }

  void Finish() final {
    cudaEventRecord(finish);
    cudaEventSynchronize(finish);
  }

  double GetIntervalTime() final {
    float interval_in_milliseconds;
    cudaEventElapsedTime(&interval_in_milliseconds, start, finish);
    return interval_in_milliseconds;
  };

  ~Timer() {
    cudaEventDestroy(start);
    cudaEventDestroy(finish);
  }
 private:
  cudaEvent_t start{};
  cudaEvent_t finish{};
};
}

#endif //CMAKE_SRC_GPU_TIMER_CUH_
