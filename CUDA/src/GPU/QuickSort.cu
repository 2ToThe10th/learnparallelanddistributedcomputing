//
// Created by 2ToThe10th on 10.10.2020.
//

#include <iostream>
#include <utility>
#include <vector>

#include "ScanUtils.cuh"
#include "QuickSort.h"
#include "DeviceIntArrayUniquePtr.cuh"
#include "Timer.cuh"
#include "Constant.h"
#include "RearrangeArrayByScan.cuh"
#include "BitonicSort.cuh"

namespace GPU {

void QuickSortByCycle(int *array, unsigned int size, int *helper_array, int *prefix_helper_array_leq);

double QuickSort(int *array, unsigned int size) {
  DeviceIntArrayUniquePtr device_array(size, array);
  DeviceIntArrayUniquePtr device_helper_array(size);
  DeviceIntArrayUniquePtr device_prefix_helper_array_leq(size);

  Timer timer;
  timer.Start();
  QuickSortByCycle(device_array.GetPointer(),
                   size,
                   device_helper_array.GetPointer(),
                   device_prefix_helper_array_leq.GetPointer());
  timer.Finish();

  device_array.CopyToHostArray(array);

  return timer.GetIntervalTime();
}

inline int FindPartition(int *array, unsigned int size);

inline unsigned GetArrayPartition(int *prefix_helper_array_leq, unsigned int size) {
  unsigned number_leq_elem; // = size - prefix_helper_array_leq[size - 1];

  cudaMemcpy(&number_leq_elem, &prefix_helper_array_leq[size - 1], sizeof(unsigned int), cudaMemcpyDeviceToHost);

  return number_leq_elem;
}

struct Task {
  Task(unsigned int shift, unsigned int size) : shift(shift), size(size) {}
  unsigned int shift;
  unsigned int size;
  int partition = 0;
};

template<unsigned number_of_stream>
class CudaStreamQueue {
 public:
  CudaStreamQueue() {
    streams = new cudaStream_t[number_of_stream];
    for (int i = 0; i < number_of_stream; ++i) {
      cudaStreamCreate(&streams[i]);
    }
  }

  cudaStream_t &GetStream() {
    ++current_index;
    if (current_index >= number_of_stream) {
      current_index = 0;
    }
    return streams[current_index];
  }

  void Synchronize() {
    for (int i = 0; i < number_of_stream; ++i) {
      cudaStreamSynchronize(streams[i]);
    }
  }

  ~CudaStreamQueue() {
    for (int i = 0; i < number_of_stream; ++i) {
      cudaStreamDestroy(streams[i]);
    }
    delete[] streams;
  }
 private:
  cudaStream_t *streams;
  unsigned current_index = number_of_stream;
};

void QuickSortByCycle(int *array, unsigned int size, int *helper_array, int *prefix_helper_array_leq) {
  std::vector<Task> task_queue;
  task_queue.emplace_back(0, size);

  CudaStreamQueue<8> streams;

  cudaStream_t non_blocking_stream;

  cudaStreamCreateWithFlags(&non_blocking_stream, cudaStreamNonBlocking);

  while (!task_queue.empty()) {
    std::vector<Task> next_iteration_task_queue;

    for (auto &task: task_queue) {

      if (task.size <= kMaxBlockSize) {
        BitonicSort<<<1, kMaxBlockSize, 0, non_blocking_stream>>>(array + task.shift, task.size);
        continue;
      }

      int *task_array = array + task.shift;
      unsigned int task_size = task.size;
      int *task_prefix_helper_array_leq = prefix_helper_array_leq + task.shift;
      int *task_scan_helper_array = helper_array + task.shift;

      task.partition = FindPartition(task_array, task_size);

      ScanLeq(task_array,
              task_size,
              task_prefix_helper_array_leq,
              task.partition,
              task_scan_helper_array,
              streams.GetStream());
    }

    streams.Synchronize();

    for (auto &task: task_queue) {

      if (task.size <= kMaxBlockSize) {
        continue;
      }

      int *task_array = array + task.shift;
      unsigned int task_size = task.size;
      int *task_helper_array = helper_array + task.shift;
      int *task_prefix_helper_array_leq = prefix_helper_array_leq + task.shift;

      int number_leq_elem = GetArrayPartition(task_prefix_helper_array_leq, task_size);

      unsigned number_of_blocks = (task_size + kMaxBlockSize - 1) / kMaxBlockSize;
      cudaStream_t &stream = streams.GetStream();
      RearrangeArrayByScan<<<number_of_blocks, kMaxBlockSize, 0, stream>>>(task_array,
                                                                           task_helper_array,
                                                                           task_prefix_helper_array_leq,
                                                                           task_size,
                                                                           task.partition);

      Copy<<<number_of_blocks, kMaxBlockSize, 0, stream>>>(task_helper_array, task_array, task_size);

      {
        unsigned size_left_part = number_leq_elem;
        if (size_left_part == task_size) {
          --size_left_part;
        }
        if (size_left_part > 1) {
          next_iteration_task_queue.emplace_back(task.shift, size_left_part);
        }
      }

      {
        unsigned size_right_part = task_size - number_leq_elem;
        if (size_right_part > 1) {
          next_iteration_task_queue.emplace_back(task.shift + number_leq_elem, size_right_part);
        }
      }
    }

    task_queue = std::move(next_iteration_task_queue);
    streams.Synchronize();
  }
  cudaDeviceSynchronize();
}

int FindPartition(int *array, unsigned int size) {
  int first; // = array[0];
  int last; // = array[size - 1];
  cudaMemcpy(&first, &array[0], sizeof(int), cudaMemcpyDeviceToHost);
  cudaMemcpy(&last, &array[size - 1], sizeof(int), cudaMemcpyDeviceToHost);
  int sum_first_and_last = first + last;
  if (sum_first_and_last < 0) {
    --sum_first_and_last;
  }
  return sum_first_and_last / 2;
}

}
