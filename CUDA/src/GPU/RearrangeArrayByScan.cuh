//
// Created by 2ToThe10th on 15.10.2020.
//

#ifndef CMAKE_SRC_GPU_REARRANGEARRAYBYSCAN_CUH_
#define CMAKE_SRC_GPU_REARRANGEARRAYBYSCAN_CUH_

namespace GPU {

__global__
void RearrangeArrayByScan(int *in_array, int *out_array, int *prefix_helper_array_leq, int size, int partition);

__global__
void Copy(int* in_array, int* out_array, int size);

};

#endif //CMAKE_SRC_GPU_REARRANGEARRAYBYSCAN_CUH_
