//
// Created by 2ToThe10th on 15.10.2020.
//

#ifndef CMAKE_SRC_GPU_SCANUTILS_CUH_
#define CMAKE_SRC_GPU_SCANUTILS_CUH_

namespace GPU {


void ScanLess(int *array,
              unsigned int size,
              int *answer_array,
              int partition,
              int *helper_scan_array,
              cudaStream_t &stream);
void ScanLeq(int *array,
             unsigned int size,
             int *answer_array,
             int partition,
             int *helper_scan_array,
             cudaStream_t &stream);

}

#endif //CMAKE_SRC_GPU_SCANUTILS_CUH_
