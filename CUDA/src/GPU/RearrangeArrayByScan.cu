//
// Created by 2ToThe10th on 15.10.2020.
//

#include "RearrangeArrayByScan.cuh"

namespace GPU {

__global__ void RearrangeArrayByScan(int *in_array,
                                     int *out_array,
                                     int *prefix_helper_array_leq,
                                     int size,
                                     int partition) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;

  if (index < size) {
    if (in_array[index] <= partition) {
      out_array[prefix_helper_array_leq[index] - 1] = in_array[index];
    } else {
      out_array[size - 1 - index + prefix_helper_array_leq[index]] = in_array[index];
    }
  }
}

__global__ void Copy(int *in_array, int *out_array, int size) {
  int index = blockIdx.x * blockDim.x + threadIdx.x;
  if (index < size) {
    out_array[index] = in_array[index];
  }
}

}