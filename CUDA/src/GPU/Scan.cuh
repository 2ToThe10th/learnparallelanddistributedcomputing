//
// Created by 2ToThe10th on 16.10.2020.
//

#ifndef CMAKE_SRC_GPU_SCAN_CUH_
#define CMAKE_SRC_GPU_SCAN_CUH_

namespace GPU {

void Scan(int *array, unsigned int size, int *helper_array, cudaStream_t& stream);

}

#endif //CMAKE_SRC_GPU_SCAN_CUH_
