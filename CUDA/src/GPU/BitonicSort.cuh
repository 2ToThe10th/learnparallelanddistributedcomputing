//
// Created by 2ToThe10th on 19.10.2020.
//

#ifndef CMAKE_SRC_GPU_BITONICSORT_CUH_
#define CMAKE_SRC_GPU_BITONICSORT_CUH_

#include "Constant.h"
#include <iostream>

namespace GPU {

__global__ void BitonicSort(int *d_array, unsigned int size);

};

#endif //CMAKE_SRC_GPU_BITONICSORT_CUH_
