//
// Created by 2ToThe10th on 15.10.2020.
//

#ifndef CMAKE_SRC_GPU_DEVICEINTARRAYUNIQUEPTR_CUH_
#define CMAKE_SRC_GPU_DEVICEINTARRAYUNIQUEPTR_CUH_

#include <cuda_runtime.h>

namespace GPU {
class DeviceIntArrayUniquePtr {
 public:
  explicit DeviceIntArrayUniquePtr(unsigned int size) : size(size) {
    if (cudaMalloc(reinterpret_cast<void **>(&array), sizeof(int) * this->size) != cudaSuccess) {
      std::cerr << "cudaMalloc" << std::endl;
      exit(1);
    }
  }

  DeviceIntArrayUniquePtr(unsigned int size, int *host_array) : size(size) {
    if (cudaMalloc(reinterpret_cast<void **>(&array), sizeof(int) * this->size) != cudaSuccess) {
      std::cerr << "cudaMalloc" << std::endl;
      exit(1);
    }
    cudaMemcpy(array, host_array, sizeof(int) * this->size, cudaMemcpyHostToDevice);
  }

  DeviceIntArrayUniquePtr &operator=(const DeviceIntArrayUniquePtr &rhs) = delete;
  DeviceIntArrayUniquePtr &operator=(DeviceIntArrayUniquePtr &&rhs) = delete;

  int *GetPointer() {
    return array;
  }

  void CopyToHostArray(int *host_array) {
    cudaMemcpy(host_array, array, sizeof(int) * size, cudaMemcpyDeviceToHost);
  }

  ~DeviceIntArrayUniquePtr() {
    if (array != nullptr) {
      cudaFree(array);
    }
  }

 private:
  int *array{};
  unsigned size = 0;
};
}

#endif //CMAKE_SRC_GPU_DEVICEINTARRAYUNIQUEPTR_CUH_
