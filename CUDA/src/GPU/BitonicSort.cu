//
// Created by 2ToThe10th on 19.10.2020.
//

#include "BitonicSort.cuh"
#include "Constant.h"
#include <iostream>

#define kMaxInteger (int)0x7fffffff;

namespace GPU {
__global__ void BitonicSort(int *d_array, unsigned int size) {
  __shared__ int shared[kMaxBlockSize];
  const unsigned index = threadIdx.x;

  if (index < size) {
    shared[index] = d_array[index];
  } else {
    shared[index] = kMaxInteger;
  }

  __syncthreads();

  for (unsigned k = 2; k <= kMaxBlockSize; k <<= 1) {
    for (unsigned j = k >> 1; j > 0; j >>= 1) {
      const unsigned swap_index = index ^ j;
      const int my_elem = shared[index];
      const int swap_elem = shared[swap_index];

      __syncthreads();

      unsigned int ascend = k * (swap_index < index);
      unsigned int descend = k * (swap_index > index);
      bool swap = false;

      if ((index & k) == ascend) {
        if (my_elem > swap_elem) {
          swap = true;
        }
      }

      if ((index & k) == descend) {
        if (swap_elem > my_elem) {
          swap = true;
        }
      }

      if (swap) {
        shared[swap_index] = my_elem;
      }

      __syncthreads();
    }
  }

  if (index < size) {
    d_array[index] = shared[index];
  }
}

}