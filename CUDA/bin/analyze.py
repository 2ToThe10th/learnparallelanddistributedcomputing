from tqdm import tqdm
import subprocess
import matplotlib.pyplot as plt
import sys
import re

if len(sys.argv) < 2:
    print("Please write path to tested application as second parameter")

application_path = sys.argv[1]

number_of_element_tests = [10, 1e2, 1e3, 1e4, 1e5, 1e6, 2e6, 6e6, 1e7, 3e7, 6e7, 1e8, 2e8, 3e8, 4e8, 5e8]

number_of_element_tests = [int(i) for i in number_of_element_tests]

time_gpu_sort_find = re.compile(r'GPU Quick Sort only computations: ([+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?) ms')
time_cpu_sort_find = re.compile(r'CPU Quick Sort: ([+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?) ms')
time_std_sort_find = re.compile(r'STD Sort: ([+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?) ms')
error_find = re.compile(r'ERROR')

gpu_sort_time = []
cpu_sort_time = []
std_sort_time = []

for number_of_element in tqdm(number_of_element_tests):
    result = subprocess.run([application_path, str(number_of_element)], capture_output=True)
    application_stdout = str(result.stdout)[2:-1]
    gpu_sort_time.append(float(time_gpu_sort_find.search(application_stdout).group(1)))
    cpu_sort_time.append(float(time_cpu_sort_find.search(application_stdout).group(1)))
    std_sort_time.append(float(time_std_sort_find.search(application_stdout).group(1)))
    number_of_error = len(error_find.findall(application_stdout))
    if number_of_error:
        print("ERROR Found")
        exit(1)

plt.plot(number_of_element_tests, gpu_sort_time, label="gpu sort")
plt.plot(number_of_element_tests, cpu_sort_time, label="cpu sort")
plt.plot(number_of_element_tests, std_sort_time, label="std sort")

plt.title("Time dependence on the number of elements")
plt.xlabel("number of elements to sort")
plt.xscale("log")
plt.ylabel("time in ms")
plt.yscale("log")
plt.legend()

plt.savefig("../plot/SortStatisticAll")
plt.clf()

give_to_second_plot = 5

plt.plot(number_of_element_tests[-give_to_second_plot:], gpu_sort_time[-give_to_second_plot:], label="gpu sort")
plt.plot(number_of_element_tests[-give_to_second_plot:], cpu_sort_time[-give_to_second_plot:], label="cpu sort")
plt.plot(number_of_element_tests[-give_to_second_plot:], std_sort_time[-give_to_second_plot:], label="std sort")

plt.title("Time dependence on the number of elements for big array")
plt.xlabel("number of elements to sort")
plt.ylabel("time in ms")
plt.legend()

plt.savefig("../plot/SortStatisticBigValue")
plt.clf()
