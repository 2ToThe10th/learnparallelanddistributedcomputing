# Quick Sort on GPU

In this project, we compare integer sorting on GPUs with that on CPUs. For CPU-based sorting, we utilize the classic quick sort and the standard library's std::sort. For GPU-based sorting, we implement a variant of quick sort. In each iteration, we efficiently determine the position for each element. We construct an array where, at the i-th position, there is a 1 if the corresponding number in the original array is less than the pivot, and 0 otherwise. We then generate a prefix sum array using the [scan algorithm](https://developer.nvidia.com/gpugems/gpugems3/part-vi-gpu-computing/chapter-39-parallel-prefix-sum-scan-cuda). This array helps us ascertain the correct position for each element smaller than the pivot after a standard quick sort iteration. A similar approach is applied for elements greater than the pivot, but in this case, we build the prefix sum array in reverse order. When the intervals in the quick sort arrays become sufficiently small, we switch to using [bitonic sort](https://en.wikipedia.org/wiki/Bitonic_sorter) for more efficient processing.

-------

Plots are located in the __plot__ folder

* In __SortStatisticAll__ there is a dependency of the runtime of GPU and CPU sorts on the number of elements with a logarithmic scale.
* In __SortStatisticBigValue__ there is a dependency of the runtime of GPU and CPU sorts on a large number of elements from the number of elements with a normal scale.

-----------------------

The program code is located in __src__

In __src/GPU__ located the sorting code.

Commands for running:

```cmake && make && ./CUDA```

------------------------

Auxiliary scripts are located in __bin__

* __analyze.py__ - run the sorting for different numbers of elements, check the answers, and draw graphs.

------------------------

### Analysis of the Graphs

The graphs show that with a large number of elements, sorting on the graphics card significantly outperforms sorting on the CPU.

-----------------------

#### A Bit About Writing the Program

* Initially, I wrote a program with the usual quick sort on GPU using scan, but this implementation was about 100 times slower than sorting on the CPU.
* Then I added streams and ran some independent operations in parallel — this sped up the entire process by about 2 times.
* Later, I found Bitonic Sort, which works very well for arrays of a size not exceeding the number of threads in a block (1024) and used it for small arrays, allowing my program to work faster than sorting on the GPU.
* Next, I added an optimization for scan, which reduces the number of bank conflicts, and this sped up the process by about 1.25 times.
* In the end, I got a quite fast-working algorithm.

----------

# Быстрая сортировка на видеокарте

Графики лежат в папке __plot__

* в __SortStatisticAll__ зависимость времени работы gpu и cpu сортировок от количества елементов с логарифмической
  шкалой
* в __SortStatisticBigValue__ зависимость времени работы gpu и cpu сортировок на большом количестве элементов от
  количества елементов с обычной шкалой

-----------------------

Код программы лежит в __src__

в __src/GPU__ лежит код сортировки

Команды для запуска:

```cmake && make && ./CUDA```

------------------------

Вспомогательные скрипты находятся в __bin__

* __analyze.py__ - запустить сортировку для разного количества элементов, проверить ответы и нарисовать графики

------------------------

### Анализ графиков

На графиках видно, что при большом количестве элементов сортировка на видеокарте сильно обгоняет сортировку на CPU

-----------------------

#### Немного о написании программы

* Для начала, я написал программу с обычным quick sort на gpu со scan, но данная реализация работала примерно в 100 раз
  дольше чем сортировка на cpu
* Далее я добавил стримы и запускал некоторые независимые операции параллельно — это ускорило работу всего в ~ 2 раза
* Потом я нашел Битонную сортировку, которая очень хорошо работает для массивов размера не больше количества нитей в
  блоке (1024) и запускал ее для маленьких массивов, это позволило моей программе работать быстрее чем сортировка на gpu
* Далее я добавил оптимизацию для scan, в которой уменьшается количество bank conflict и это ускорило примерно в 1,25
  раз
* В итоге получился достаточно быстроработающий алгоритм