import re
import os
import math

time_find = re.compile('Time: ([+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)')
result_find = re.compile('Result on MPI: 3.14159')

print("{:<10}{:<4}{:<4}{:<4}{}".format("segments", "P*T", "P", "T", "time"))

for number_of_segment in [1000, 1000000, 100000000]:
    for number_of_processes in range(1, 9):
        for number_of_threads in range(1, math.ceil(9 / number_of_processes)):
            format_dict = {'number_of_processes': number_of_processes, 'number_of_segment': number_of_segment,
                           'number_of_threads': number_of_threads}
            with open(
                    'out/out_{number_of_segment}_{number_of_processes}_{number_of_threads}.txt'.format(**format_dict),
                    'r') as f:
                is_time_found = False
                is_result_found = False
                for line in f:
                    time = time_find.match(line)
                    if time is not None:
                        is_time_found = True
                        print("{:<10}{:<4}{:<4}{:<4}{}".format(number_of_segment,
                                                               number_of_processes * number_of_threads,
                                                               number_of_processes,
                                                               number_of_threads, time.group(1)))

                    result = result_find.match(line)
                    if result is not None:
                        is_result_found = True

                if not (is_time_found and is_result_found):
                    raise ValueError(
                        'out/out_{number_of_segment}_{number_of_processes}_{number_of_threads}.txt'.format(
                            **format_dict))
