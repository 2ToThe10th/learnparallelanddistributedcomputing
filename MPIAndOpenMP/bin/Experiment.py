import re


class Experiment:

    def __init__(self, line):
        line_re = re.compile("(\d+)[ ]+\d+[ ]+(\d+)[ ]+(\d+)[ ]+([+-]?(\d+(\.\d*)?|\.\d+)([eE][+-]?\d+)?)")
        parsed = line_re.match(line)
        if parsed is None:
            raise ValueError("line incorrect")
        self.__initialize(parsed.group(1), parsed.group(2), parsed.group(3), parsed.group(4))

    def __initialize(self, number_of_segment,
                     number_of_processes, number_of_threads, time):
        self.number_of_segments = int(number_of_segment)
        self.number_of_processes = int(number_of_processes)
        self.number_of_threads = int(number_of_threads)
        self.time = float(time)

    def __str__(self):
        return "seq: {}, proc: {}, thread: {}, time: {}".format(self.number_of_segments,
                                                                self.number_of_processes,
                                                                self.number_of_threads,
                                                                self.time)

    def is_in_one_thread(self):
        return self.number_of_threads == 1

    def boost(self, time_on_one_process):
        return time_on_one_process / self.time

    @staticmethod
    def read_from_file(file_name):
        experiments = []

        with open(file_name, 'r') as analyze:
            is_first_line = True
            for line in analyze:
                if is_first_line:
                    is_first_line = False
                    continue

                experiments.append(Experiment(line))

        return experiments

    def number_of_cpus(self):
        return self.number_of_threads * self.number_of_processes
