from Experiment import Experiment
import matplotlib.pyplot as plt

experiments = Experiment.read_from_file("analyze")

for number_of_segments in [1000, 1000000, 100000000]:
    fig, ax = plt.subplots(nrows=7, ncols=1, figsize=(10, 40))

    for cpu in range(2, 9):
        experiments_to_graphs = []

        for experiment in experiments:
            if experiment.number_of_cpus() == cpu and experiment.number_of_segments == number_of_segments:
                experiments_to_graphs.append([experiment.number_of_processes, experiment.time])

        experiments_to_graphs.sort()

        ax[cpu - 2].plot(["{} processes\n{} threads".format(experiment[0], cpu // experiment[0]) for experiment in
                          experiments_to_graphs],
                         [experiment[1] for experiment in experiments_to_graphs])

        ax[cpu - 2].set_title("{} cpus".format(cpu), fontsize=20)
        ax[cpu - 2].set_ylabel("time")
        ax[cpu -2].set_ylim(ymin=0)

    fig.suptitle("{} segments".format(number_of_segments), fontsize=25)

    plt.savefig("../graphs/OpenMP_Statistic/{}_segments.png".format(number_of_segments))
