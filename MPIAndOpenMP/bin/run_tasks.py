import os
import math

template = """\
#!/bin/bash

#SBATCH --ntasks={number_of_processes}
#SBATCH --cpus-per-task={number_of_threads}
#SBATCH --job-name=homework1_{number_of_processes}_{number_of_threads}_{number_of_segment}
#SBATCH --comment="Homework1 on {number_of_processes} processes with {number_of_threads} in each and {number_of_segment} segments"
#SBATCH --output=out/out_{number_of_segment}_{number_of_processes}_{number_of_threads}.txt
#SBATCH --error=error/error_{number_of_segment}_{number_of_processes}_{number_of_threads}.txt
mpiexec ../cmake-build/Task1 {number_of_segment} {number_of_threads}
"""

def create_dir(names):
    for name in names:
        try:
            os.mkdir(name, 0o740)
        except FileExistsError:
            pass

create_dir(['run', 'out', 'error'])

for number_of_segment in [1000, 1000000, 100000000]:
    for number_of_processes in range(1, 9):
        for number_of_threads in range(1, math.ceil(9 / number_of_processes)):
            format_dict = {'number_of_processes': number_of_processes, 'number_of_segment': number_of_segment, 'number_of_threads': number_of_threads}
            with open('run/run_{number_of_segment}_{number_of_processes}_{number_of_threads}.sh'.format(**format_dict), 'w') as f:
                f.write(template.format(**format_dict))
            os.system('sbatch run/run_{number_of_segment}_{number_of_processes}_{number_of_threads}.sh'.format(**format_dict))
