from Experiment import Experiment
import matplotlib.pyplot as plt

experiments = Experiment.read_from_file("analyze")

for number_of_segments in [1000, 1000000, 100000000]:

    experiments_process = []
    experiments_boost = []

    time_on_one_process = 1

    for experiment in experiments:
        if experiment.is_in_one_thread() and experiment.number_of_segments == number_of_segments \
                and experiment.number_of_processes == 1:
            time_on_one_process = experiment.time

    for experiment in experiments:
        if experiment.is_in_one_thread() and experiment.number_of_segments == number_of_segments:
            experiments_process.append(experiment.number_of_processes)
            experiments_boost.append(experiment.boost(time_on_one_process))

    fig, ax = plt.subplots()
    ax.plot(experiments_process, experiments_boost)

    plt.title("{} segments".format(number_of_segments))
    ax.set_xlabel("number of processes")
    ax.set_ylabel("boost")
    ax.set_ylim(ymin=0)

    plt.savefig("../graphs/MPI_Statistic/{}_segments.png".format(number_of_segments))
