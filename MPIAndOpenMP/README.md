# Нахождение интеграла с использованием MPI

### Задание:

Решить определенный интеграл методом трапеций.

[full task exercise](https://gitlab.com/pd2020-supplementary/8xx-GLOBAL/-/blob/master/task1_mpi.md)

----------------------

Графики лежат в папке __graphs__

* в __MPI_Statistic__ зависимость ускорения от количества процессоров при запусках с одним потоком
* в __OpenMP_Statistic__ зависимость времени от замены процессов на потоки

----------------------

Код программы лежит в __src__

Команды для запуска:

```cmake && make && ./Task1```

----------------------

Вспомогательные скрипты находятся в __bin__

* __run_tasks.py__ - запуск задач на кластере
* __analyze.py__ - вытащить результаты из __out__ файлов и проверить ответы
* __analyze__ - результаты __analyze.py__
* __create_mpi_graphs.py__ и __create_openmp_graphs.py__ - для создания графиков из __analyze__

------------------------

### Анализ графиков:

На графиках из __MPI_Statistic__ видно, что при 1000 отрезках деление задачи на процессы только уменьшает скорость из-за
пересылки данных, но при 1e8 отрезках скорость возрастает почти линейно

На графиках из __OpenMP_Statistic__ видно, что на данном кластере предпочтительно использовать процессы при большом
количестве операций в программе
