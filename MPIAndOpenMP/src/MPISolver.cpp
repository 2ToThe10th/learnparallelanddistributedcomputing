//
// Created by 2ToThe10th on 19.09.2020.
//

#include <mpi.h>
#include "MPISolver.h"
#include "CalculateIntegralWithOpenMP.h"

MPISolver::MPISolver(int *argc, char ***argv, int number_of_threads) : number_of_threads(number_of_threads) {
  MPI_Init(argc, argv);

  MPI_Comm_size(MPI_COMM_WORLD, &world_size);
  MPI_Comm_rank(MPI_COMM_WORLD, &rank);
}

MPISolver::CalculatedIntegral MPISolver::CalculateIntegralOnMPI(const std::function<long double(long double)> &function,
                                                                long double from,
                                                                long double to,
                                                                unsigned long long int number_of_segments) {
  auto border = SetSegment(from, to, number_of_segments);

  auto segment_integral =
      CalculateIntegralWithOpenMP(function,
                                  border.GetFrom(),
                                  border.GetTo(),
                                  border.GetNumberOfSegments(),
                                  number_of_threads);

  if (rank == kMainProcess) {
    auto answer = SumUpOtherPartsOfIntegral() + segment_integral;
    return {answer, segment_integral};
  } else {
    SendLongDoubleToMainProcess(&segment_integral, 1);
    return CalculatedIntegral(segment_integral);
  }
}

int MPISolver::ProcessRank() const {
  return rank;
}

MPISolver::~MPISolver() {
  MPI_Finalize();
}

inline MPISolver::Segment MPISolver::SetSegment(long double from,
                                                long double to,
                                                unsigned long long number_of_segments) const {

  if (rank == kMainProcess) {
    unsigned long long segments_for_process = number_of_segments / world_size;
    unsigned long long remain_segments = number_of_segments % world_size;
    unsigned long long current_position = segments_for_process;
    long double delta = (to - from) / number_of_segments;

    long double border[2] = {from + delta * current_position, 0};
    for (int i = 1; i < world_size; ++i) {
      auto segments_for_i_process = segments_for_process;
      if (i <= remain_segments) {
        ++segments_for_i_process;
      }
      current_position += segments_for_i_process;
      border[1] = from + current_position * delta;
      ISendLongDoubleToProcess(i, border, 2);
      ISendUnsignedLongLongToProcess(i, &segments_for_i_process, 1);
      border[0] = border[1];
    }

    return {from, from + segments_for_process * delta, segments_for_process};
  } else {
    long double recv[2];
    RecvLongDoubleFromMainProcess(recv, 2);
    unsigned long long number_of_segments;
    RecvUnsignedLongLongFromMainProcess(&number_of_segments, 1);
    return {recv[0], recv[1], number_of_segments};
  }
}

inline void MPISolver::RecvLongDoubleFromMainProcess(long double *buf, int count) {
  MPI_Recv(buf, count, MPI_LONG_DOUBLE, kMainProcess, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

inline void MPISolver::RecvUnsignedLongLongFromMainProcess(unsigned long long int *buf, int count) {
  MPI_Recv(buf, count, MPI_UNSIGNED_LONG_LONG, kMainProcess, MPI_ANY_TAG, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
}

inline void MPISolver::ISendLongDoubleToProcess(int process_id, long double *buf, int count) {
  MPI_Request request;
  MPI_Isend(buf, count, MPI_LONG_DOUBLE, process_id, 0, MPI_COMM_WORLD, &request);
  MPI_Request_free(&request);
}

inline void MPISolver::ISendUnsignedLongLongToProcess(int process_id, unsigned long long int *buf, int count) {
  MPI_Request request;
  MPI_Isend(buf, count, MPI_UNSIGNED_LONG_LONG, process_id, 0, MPI_COMM_WORLD, &request);
  MPI_Request_free(&request);
}

inline void MPISolver::SendLongDoubleToMainProcess(const long double *buf, int count) {
  MPI_Send(buf, count, MPI_LONG_DOUBLE, kMainProcess, 0, MPI_COMM_WORLD);
}

long double MPISolver::SumUpOtherPartsOfIntegral() const {
  long double integrals[world_size - 1];

  MPI_Request requests[world_size - 1];

  for (int i = 0; i < world_size - 1; ++i) {
    MPI_Irecv(integrals + i, 1, MPI_LONG_DOUBLE, i + 1, MPI_ANY_TAG, MPI_COMM_WORLD, requests + i);
  }
  MPI_Waitall(world_size - 1, requests, MPI_STATUSES_IGNORE);

  long double sum = 0;
  for (int i = 0; i < world_size - 1; ++i) {
    sum += integrals[i];
  }
  return sum;
}

void MPISolver::Barrier() const {
  MPI_Barrier(MPI_COMM_WORLD);
}

double MPISolver::GetTime() const {
  return MPI_Wtime();
}

