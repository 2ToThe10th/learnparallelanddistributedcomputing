//
// Created by 2ToThe10th on 20.09.2020.
//

#ifndef TASK1_SRC_CALCULATEINTEGRAL_H_
#define TASK1_SRC_CALCULATEINTEGRAL_H_
#include <functional>


long double CalculateIntegral(const std::function<long double(long double)> &function,
                                        long double from,
                                        long double to,
                                        unsigned long long int number_of_segments) {
  long double integral = 0;
  const long double kDelta = (to - from) / number_of_segments;
  const long double kStart = from + kDelta / 2;

  for (int i = 0; i < number_of_segments; ++i) {
    integral += function(kStart + i * kDelta) * kDelta;
  }

  return integral;
}

#endif //TASK1_SRC_CALCULATEINTEGRAL_H_
