//
// Created by 2ToThe10th on 19.09.2020.
//

#ifndef TASK1_SRC_CALCULATEINTEGRALWITHOPENMP_H_
#define TASK1_SRC_CALCULATEINTEGRALWITHOPENMP_H_
#include <functional>

long double CalculateIntegralWithOpenMP(const std::function<long double(long double)> &function,
                                        long double from,
                                        long double to,
                                        unsigned long long int number_of_segments,
                                        int number_of_threads);

#endif //TASK1_SRC_CALCULATEINTEGRALWITHOPENMP_H_
