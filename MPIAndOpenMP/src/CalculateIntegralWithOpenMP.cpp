//
// Created by 2ToThe10th on 19.09.2020.
//

#include "CalculateIntegralWithOpenMP.h"
#include <omp.h>
#include <iostream>
#include <mpi.h>

long double CalculateIntegralWithOpenMP(const std::function<long double(long double)> &function,
                                        long double from,
                                        long double to,
                                        unsigned long long int number_of_segments,
                                        int number_of_threads) {

  long double integral = 0;

  #pragma omp parallel default(shared) num_threads(number_of_threads)
  {
    long double integral_thread = 0;
    const long double kDelta = (to - from) / number_of_segments;
    const long double kStart = from + kDelta / 2;

    #pragma omp for nowait
    for (int i = 0; i < number_of_segments; ++i) {
      integral_thread += function(kStart + i * kDelta) * kDelta;
    }

    #pragma omp atomic
    integral += integral_thread;
  }

  return integral;
}