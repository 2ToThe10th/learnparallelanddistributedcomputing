//
// Created by 2ToThe10th on 19.09.2020.
//

#ifndef TASK1_SRC_MPISOLVER_H_
#define TASK1_SRC_MPISOLVER_H_
#include <functional>

class MPISolver {
 public:
  MPISolver(int *argc, char ***argv, int number_of_threads);

  class CalculatedIntegral {
   public:
    CalculatedIntegral(long double answer, long double segment_answer)
        : is_main_process(true), answer(answer), segment_answer(segment_answer) {

    }

    explicit CalculatedIntegral(long double segment_answer)
        : is_main_process(false), answer(0), segment_answer(segment_answer) {

    }

    [[nodiscard]] long double GetSegmentAnswer() const {
      return segment_answer;
    }

    explicit operator bool() const {
      return is_main_process;
    }

    long double operator*() const {
      return answer;
    }

   private:
    bool is_main_process;
    long double answer;
    long double segment_answer;
  };

  CalculatedIntegral CalculateIntegralOnMPI(const std::function<long double(long double)> &function,
                                            long double from,
                                            long double to,
                                            unsigned long long int number_of_segments);

  [[nodiscard]] int ProcessRank() const;

  void Barrier() const;

  double GetTime() const;

  ~MPISolver();

 private:
  static void RecvLongDoubleFromMainProcess(long double *buf, int count);
  static void RecvUnsignedLongLongFromMainProcess(unsigned long long *buf, int count);
  static void SendLongDoubleToMainProcess(const long double *buf, int count);
  static void ISendLongDoubleToProcess(int process_id, long double *buf, int count);
  static void ISendUnsignedLongLongToProcess(int process_id, unsigned long long int *buf, int count);

  class Segment {
   public:
    Segment(long double from, long double to,
            unsigned long long number_of_segments) :
        from(from),
        to(to),
        number_of_segments(number_of_segments) {
    }

    long double GetFrom() const {
      return from;
    }
    long double GetTo() const {
      return to;
    }
    unsigned long long int GetNumberOfSegments() const {
      return number_of_segments;
    }

   private:
    long double from;
    long double to;
    unsigned long long number_of_segments;
  };

  Segment SetSegment(long double from, long double to,
                     unsigned long long number_of_segments) const;

  long double SumUpOtherPartsOfIntegral() const;

 private:
  int rank = 0;
  int world_size = 0;
  int number_of_threads = 0;
  static constexpr int kMainProcess = 0;
};

#endif //TASK1_SRC_MPISOLVER_H_
