//
// Created by 2ToThe10th on 19.09.2020.
//
#include "CalculateIntegralWithOpenMP.h"
#include "MPISolver.h"
#include "CalculateIntegral.h"
#include <iostream>
#include <ctime>
#include <chrono>

long double Function(long double x) {
  return 4 / (1 + x * x);
}

int main(int argc, char **argv) {

#ifndef _OPENMP
  "No OpenMP"
#endif

  if (argc < 3) {
    std::cout << "Too less arguments" << std::endl;
    return 1;
  }

  unsigned long long number_of_segments = strtoull(argv[1], nullptr, 10);
  int number_of_threads = strtol(argv[2], nullptr, 10);

  if (number_of_segments == 0) {
    std::cout << "Number of segments might be integer more than 0" << std::endl;
    return 2;
  }

  if (number_of_threads == 0) {
    std::cout << "Number of threads might be integer more than 0" << std::endl;
    return 3;
  }

  MPISolver mpi(&argc, &argv, number_of_threads);
  auto start_time = mpi.GetTime();

  constexpr long double kIntegralFrom = 0;
  constexpr long double kIntegralTo = 1;

  auto calculated = mpi.CalculateIntegralOnMPI(Function, kIntegralFrom, kIntegralTo, number_of_segments);

  if (calculated) {
    mpi.Barrier();
    std::cout.precision(25);

    auto finish_time = mpi.GetTime();
    std::cout << "Result on " << mpi.ProcessRank() << " process : " << calculated.GetSegmentAnswer() << std::endl;
    std::cout << "Result on MPI: " << *calculated << std::endl;
    std::cout << "Time: " << finish_time - start_time << std::endl;
    std::cout << "Result on One Process: "
              << CalculateIntegral(Function, kIntegralFrom, kIntegralTo, number_of_segments)
              << std::endl;
  } else {
    mpi.Barrier();
    std::cout.precision(25);
    std::cout << "Result on " << mpi.ProcessRank() << " process : " << calculated.GetSegmentAnswer() << std::endl;
  }

  return 0;
}
