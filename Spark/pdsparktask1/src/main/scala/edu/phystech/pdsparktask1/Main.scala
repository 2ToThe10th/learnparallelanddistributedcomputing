package edu.phystech.pdsparktask1

import org.apache.spark.{SparkConf, SparkContext}

object Main {
  def main(args: Array[String]): Unit = {
    if (args.isEmpty) {
      println("Write word to find bigram with")
      return
    }
    val wordToFind = args(0).toLowerCase()
    val conf = new SparkConf().setAppName("Count narodnaya bigrams").setMaster("yarn")
    val sc = new SparkContext(conf)
    val data = sc.textFile("/data/wiki/en_articles_part")
    val parsedBigram = data.flatMap(line => {
      val words = "[a-zA-Z]+".r.findAllIn(line).toArray.map(_.toLowerCase());
      (0 to (words.length - 2)).filter(wordIndex => words(wordIndex) == wordToFind)
        .map(wordIndex => words(wordIndex) + "_" + words(wordIndex + 1))
    })
    val groupedWithInputNumber = parsedBigram.map(bigram => (bigram, 1)).reduceByKey(_ + _).sortByKey()
    groupedWithInputNumber.collect().foreach(bigram => println(bigram._1 + " " + bigram._2))
  }
}