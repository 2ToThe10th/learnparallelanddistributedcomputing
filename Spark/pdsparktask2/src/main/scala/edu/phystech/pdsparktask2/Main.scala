package edu.phystech.pdsparktask2

import org.apache.spark.{SparkConf, SparkContext}

import scala.collection.mutable.ArrayBuffer

object Main {
  def getPath(isToVertexOnIDistance: org.apache.spark.rdd.RDD[(Int, Int)],
              toVertex: Int,
              onIDistanceHistory: ArrayBuffer[org.apache.spark.rdd.RDD[(Int, Int)]]): String = {
    val vertexBeforeToVertex = isToVertexOnIDistance.take(1)(0)._2
    val path = onIDistanceHistory.scanRight(vertexBeforeToVertex)(
      (onIDistance, nextVertex) =>
        onIDistance.filter(edge => edge._1 == nextVertex).take(1)(0)._2
    )
    path.mkString("", ",", ",") + toVertex
  }

  def main(args: Array[String]): Unit = {
    if (args.length < 2) {
      println("Write from and to vertex")
      return
    }
    val fromVertex = args(0).toInt
    val toVertex = args(1).toInt
    val conf = new SparkConf().setAppName("Count narodnaya bigrams").setMaster("yarn")
    val sc = new SparkContext(conf)
    val edges = sc.textFile("/data/twitter/twitter_sample.txt").map(line => {
      val ids = line.split('\t')
      (ids(1).toInt, ids(0).toInt)
    }).cache()

    val onIDistanceHistory = new ArrayBuffer[org.apache.spark.rdd.RDD[(Int, Int)]]
    var distance = 0
    var ready = false

    while (!ready) {
      distance += 1
      val vertexOnIDistance = if (1 == distance) {
        edges.filter(edge => edge._1 == fromVertex).map(edge => (edge._2, edge._1))
      } else {
        onIDistanceHistory.last.join(edges).map(data => (data._2._2, data._1)).reduceByKey((a, _) => a)
      } cache()
      val isToVertexOnIDistance = vertexOnIDistance.filter(edge => edge._1 == toVertex).cache()
      if (isToVertexOnIDistance.isEmpty()) {
        onIDistanceHistory.append(vertexOnIDistance)
      } else {
        println(getPath(isToVertexOnIDistance, toVertex, onIDistanceHistory))
        ready = true
      }
    }
  }
}