#! /usr/bin/env bash

out_dir="word_rearrangement_statistic"
mvn > /dev/null
hadoop fs -rm -r $out_dir > /dev/null

hadoop jar target/map_reduce_task_2-1.0.jar /data/wiki/en_articles $out_dir > /dev/null &&

hadoop fs -cat $out_dir/part-r-00000 | head
