package edu.phystech.mapreducetask2;

import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.lib.InputSampler;
import org.apache.hadoop.mapred.lib.TotalOrderPartitioner;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.lib.input.SequenceFileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;

class WordRearrangementStatistic extends Configured implements Tool {
    @Override
    public int run(String[] args) throws InterruptedException, IOException, ClassNotFoundException {
        if (args.length < 2) {
            throw new IllegalArgumentException("input data not full");
        }
        Path inputPath = new Path(args[0]);
        Path tmpPath = new Path(args[1] + "_tmp");
        Path partitionPath = new Path(args[1] + "_partition");
        Path outputPath = new Path(args[1]);

        FileSystem fs = FileSystem.get(this.getConf());
        deleteFolder(fs, tmpPath);
        deleteFolder(fs, partitionPath);

        if (runWordRearrangementStatisticJob(inputPath, tmpPath) != 0) {
            deleteFolder(fs, tmpPath);
            return 1;
        }

        int result = runSorterAndPrettier(tmpPath, outputPath, partitionPath);
        deleteFolder(fs, tmpPath);
        deleteFolder(fs, partitionPath);
        return result;
    }

    private static void deleteFolder(FileSystem fs, Path... paths) throws IOException {
        for (Path path : paths) {
            if (fs.exists(path)) {
                fs.delete(path, true);
            }
        }
    }

    private int runWordRearrangementStatisticJob(Path inputPath, Path outputPath) throws IOException, ClassNotFoundException, InterruptedException {
        Job job = Job.getInstance(this.getConf(), "Word Rearrangement Statistic Calculate");

        job.setJarByClass(WordRearrangementStatistic.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);

        job.setMapperClass(GetRearrangementForAllWords.class);
        job.setMapOutputKeyClass(Text.class);
        job.setMapOutputValueClass(Text.class);
        job.setMaxMapAttempts(2);

        job.setReducerClass(CollectWithCommonRearrangement.class);
        job.setOutputKeyClass(WordWithNumberOfOccurrences.class);
        job.setOutputValueClass(Text.class);
        job.setNumReduceTasks(9);
        job.setMaxReduceAttempts(2);

        TextInputFormat.addInputPath(job, inputPath);
        SequenceFileOutputFormat.setOutputPath(job, outputPath);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    private int runSorterAndPrettier(Path inputPath, Path outputPath, Path partitionPath) throws IOException,
            ClassNotFoundException, InterruptedException {
        Job job = Job.getInstance(this.getConf(), "Word Sort and Prettier");

        job.setJarByClass(WordRearrangementStatistic.class);
        job.setInputFormatClass(SequenceFileInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapperClass(Mapper.class);
        job.setMapOutputKeyClass(WordWithNumberOfOccurrences.class);
        job.setMapOutputValueClass(Text.class);
        job.setMaxMapAttempts(2);

        job.setReducerClass(ReturnPrettier.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(Text.class);
        job.setNumReduceTasks(9);
        job.setMaxReduceAttempts(2);

        SequenceFileInputFormat.addInputPath(job, inputPath);
        TextOutputFormat.setOutputPath(job, outputPath);

        job.setPartitionerClass(TotalOrderPartitioner.class);

        TotalOrderPartitioner.setPartitionFile(job.getConfiguration(), partitionPath);
        InputSampler.Sampler<WordWithNumberOfOccurrences, Text> sampler = new InputSampler.RandomSampler<>(0.05, 10000
                , 10);
        InputSampler.writePartitionFile(job, sampler);

        return job.waitForCompletion(true) ? 0 : 1;
    }
}
