package edu.phystech.mapreducetask2;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.io.WritableComparable;
import org.jetbrains.annotations.NotNull;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

class WordWithNumberOfOccurrences implements WritableComparable<WordWithNumberOfOccurrences> {
    private final IntWritable numberOfOccurrences = new IntWritable();
    private Text word = new Text();

    public void setNumberOfOccurrences(int numberOfOccurrences) {
        this.numberOfOccurrences.set(numberOfOccurrences);
    }

    public void setWord(Text word) {
        this.word = word;
    }

    public IntWritable getNumberOfOccurrences() {
        return numberOfOccurrences;
    }

    public Text getWord() {
        return word;
    }

    @Override
    public int compareTo(@NotNull WordWithNumberOfOccurrences other) {
        int compareNumber = -numberOfOccurrences.compareTo(other.numberOfOccurrences);
        if (compareNumber == 0) {
            return word.compareTo(other.word);
        }
        return compareNumber;
    }

    @Override
    public void write(DataOutput dataOutput) throws IOException {
        numberOfOccurrences.write(dataOutput);
        word.write(dataOutput);
    }

    @Override
    public void readFields(DataInput dataInput) throws IOException {
        numberOfOccurrences.readFields(dataInput);
        word.readFields(dataInput);
    }

    @Override
    public String toString() {
        return "|WordWithNumberOfOccurrences: " + numberOfOccurrences.toString() + "  " + word.toString() + "|";
    }
}
