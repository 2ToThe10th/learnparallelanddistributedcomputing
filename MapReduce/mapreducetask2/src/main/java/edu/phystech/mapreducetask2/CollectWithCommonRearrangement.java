package edu.phystech.mapreducetask2;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

class CollectWithCommonRearrangement extends Reducer<Text, Text, WordWithNumberOfOccurrences, Text> {
    private static final WordWithNumberOfOccurrences wordWithNumberOfOccurrences = new WordWithNumberOfOccurrences();
    private static final Text output = new Text();
    private static final int numberOfElementInInfo = 5;

    @Override
    public void reduce(Text sortedWordText, Iterable<Text> wordTextArray, Context context) throws IOException, InterruptedException {
        int numberOfOccurrences = 0;
        Map<String, Integer> allWordsWithNumberInOccurrences = new HashMap<>();

        for (Text wordText : wordTextArray) {
            allWordsWithNumberInOccurrences.merge(wordText.toString(), 1, Integer::sum);
            ++numberOfOccurrences;
        }

        wordWithNumberOfOccurrences.setNumberOfOccurrences(numberOfOccurrences);
        wordWithNumberOfOccurrences.setWord(sortedWordText);
        output.set(TopElements.from(allWordsWithNumberInOccurrences, numberOfElementInInfo).toString());
        context.write(wordWithNumberOfOccurrences, output);
    }
}
