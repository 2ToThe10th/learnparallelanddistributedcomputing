package edu.phystech.mapreducetask2;

import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

class ReturnPrettier extends Reducer<WordWithNumberOfOccurrences, Text, Text, Text> {
    private static final Text sortedWord = new Text("1");
    private static final Text numberOfOccurrencesWithInfo = new Text("2");

    @Override
    public void reduce(WordWithNumberOfOccurrences wordWithNumberOfOccurrences, Iterable<Text> infos, Context context) throws IOException, InterruptedException {
        sortedWord.set(wordWithNumberOfOccurrences.getWord().toString());

        String numberOfOccurrencesWithInfoStr = Integer.toString(wordWithNumberOfOccurrences.getNumberOfOccurrences().get()) +
                '\t' + infos.iterator().next().toString();
        numberOfOccurrencesWithInfo.set(numberOfOccurrencesWithInfoStr);

        context.write(sortedWord, numberOfOccurrencesWithInfo);
    }
}
