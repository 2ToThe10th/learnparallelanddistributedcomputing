package edu.phystech.mapreducetask2;

import java.util.Map;

class TopElements {
    private final int numberOfElements;
    private final StringWithNumberOfInput[] array;

    private TopElements(int numberOfElements) {
        this.numberOfElements = numberOfElements;
        array = new StringWithNumberOfInput[numberOfElements];
    }

    public static TopElements from(Map<String, Integer> wordsWithNumberInOccurrences, int numberOfElements) {
        TopElements topElements = new TopElements(numberOfElements);

        for (Map.Entry<String, Integer> entry : wordsWithNumberInOccurrences.entrySet()) {
            StringWithNumberOfInput toAppend = new StringWithNumberOfInput(entry.getKey(),
                    entry.getValue());
            for (int i = 0; i < numberOfElements; ++i) {
                StringWithNumberOfInput element = topElements.array[i];
                if (element == null) {
                    topElements.array[i] = toAppend;
                    break;
                } else {
                    if (toAppend.LessThan(element)) {
                        if (numberOfElements - 1 - i >= 0) {
                            System.arraycopy(topElements.array, i, topElements.array, i + 1, numberOfElements - 1 - i);
                        }
                        topElements.array[i] = toAppend;
                        break;
                    }
                }
            }
        }
        return topElements;
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < numberOfElements; ++i) {
            if (array[i] == null) {
                break;
            }
            stringBuilder.append(array[i].toString());
            stringBuilder.append(";");
        }
        return stringBuilder.toString();
    }

    private static class StringWithNumberOfInput {
        private final String string;
        private final int numberOfInput;

        public StringWithNumberOfInput(String string, int numberOfInput) {
            this.string = string;
            this.numberOfInput = numberOfInput;
        }

        public boolean LessThan(StringWithNumberOfInput other) {
            if (numberOfInput > other.numberOfInput) {
                return true;
            } else if (numberOfInput == other.numberOfInput) {
                return string.compareTo(other.string) < 0;
            }
            return false;
        }

        @Override
        public String toString() {
            return string + ":" + numberOfInput;
        }
    }
}
