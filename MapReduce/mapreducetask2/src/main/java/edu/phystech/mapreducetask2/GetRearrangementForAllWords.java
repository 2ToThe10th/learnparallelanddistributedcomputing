package edu.phystech.mapreducetask2;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class GetRearrangementForAllWords extends Mapper<LongWritable, Text, Text, Text> {
    private static final Text sortedWord = new Text();
    private static final Text word = new Text();
    Pattern wordPattern = Pattern.compile("\\w{3,}");

    @Override
    public void map(LongWritable offset, Text line, Context context) throws IOException, InterruptedException {
        String input = line.toString();
        Matcher matcher = wordPattern.matcher(input);
        while (matcher.find()) {
            String wordStr = matcher.group().toLowerCase();
            char[] wordInArray = wordStr.toCharArray();
            Arrays.sort(wordInArray);
            String sortedWordStr = new String(wordInArray);
            sortedWord.set(sortedWordStr);
            word.set(wordStr);
            context.write(sortedWord, word);
        }
    }
}
