#! /usr/bin/env bash

out_dir="indexes_reshuffle"
mvn >/dev/null
hadoop fs -rm -r $out_dir >/dev/null

hadoop jar target/map_reduce_task_1-1.0.jar /data/ids $out_dir 5 >/dev/null &&

hadoop fs -cat $out_dir/part-r-00000 | head -n 50
