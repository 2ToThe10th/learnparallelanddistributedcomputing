package edu.phystech.mapreducetask1;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.TextOutputFormat;
import org.apache.hadoop.util.Tool;

import java.io.IOException;
import java.util.concurrent.ThreadLocalRandom;


public class IndexesReshuffle extends Configured implements Tool {

    private static final int MAX_RANDOM_KEY = (int) 3e4;
    private static final int ID_LENGTH = 18;
    private static final int MAX_ID_IN_OUTPUT_STRING = 5;
    private static final int OUTPUT_TEXT_MAX_SIZE = ID_LENGTH * MAX_ID_IN_OUTPUT_STRING + MAX_ID_IN_OUTPUT_STRING - 1;

    @Override
    public int run(String[] args) throws Exception {
        if (args.length < 3) {
            throw new IllegalArgumentException("input data not full");
        }
        Path inputPath = new Path(args[0]);
        Path outputPath = new Path(args[1]);
        int numberOfReducer = Integer.parseInt(args[2]);

        Configuration conf = this.getConf();
        Job job = Job.getInstance(conf, "Indexes reshuffle");

        job.setJarByClass(IndexesReshuffle.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputFormatClass(TextOutputFormat.class);

        job.setMapperClass(SetRandomNumberMapper.class);
        job.setMapOutputKeyClass(IntWritable.class);
        job.setMapOutputValueClass(Text.class);
        job.setMaxMapAttempts(2);

        job.setReducerClass(JoinIntoString.class);
        job.setOutputKeyClass(Text.class);
        job.setOutputValueClass(NullWritable.class);
        job.setNumReduceTasks(numberOfReducer);
        job.setMaxReduceAttempts(2);

        TextInputFormat.addInputPath(job, inputPath);
        TextOutputFormat.setOutputPath(job, outputPath);

        return job.waitForCompletion(true) ? 0 : 1;
    }

    public static class SetRandomNumberMapper extends Mapper<LongWritable, Text, IntWritable, Text> {
        private final IntWritable key = new IntWritable();

        @Override
        public void map(LongWritable offset, Text id, Context context) throws IOException, InterruptedException {
            key.set(ThreadLocalRandom.current().nextInt(0, MAX_RANDOM_KEY));
            context.write(key, id);
        }
    }

    public static class JoinIntoString extends Reducer<IntWritable, Text, Text, NullWritable> {
        private final Text idsText = new Text();

        @Override
        public void reduce(IntWritable key, Iterable<Text> ids, Context context) throws IOException, InterruptedException {
            StringBuilder stringBuilder = new StringBuilder(OUTPUT_TEXT_MAX_SIZE);
            int index_in_string = 0;
            for (Text id : ids) {
                if (index_in_string == MAX_ID_IN_OUTPUT_STRING) {
                    idsText.set(stringBuilder.toString());
                    context.write(idsText, NullWritable.get());
                    index_in_string = 0;
                    stringBuilder = new StringBuilder(OUTPUT_TEXT_MAX_SIZE);
                }
                if (index_in_string != 0) {
                    stringBuilder.append(',');
                }

                stringBuilder.append(id.toString());

                ++index_in_string;
            }
            idsText.set(stringBuilder.toString());
            context.write(idsText, NullWritable.get());
        }
    }
}
