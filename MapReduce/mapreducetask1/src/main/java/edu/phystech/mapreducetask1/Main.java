package edu.phystech.mapreducetask1;

import org.apache.hadoop.util.ToolRunner;

public class Main {
    public static void main(String[] args) throws Exception {
        ToolRunner.run(new IndexesReshuffle(), args);
    }
}
