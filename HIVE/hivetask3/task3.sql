ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;

USE gribal;

SELECT browser, sum(IF(sex = 'male', 1, 0)) as male_cnt, sum(IF(sex = 'female', 1, 0)) as female_cnt
FROM Users
         inner join
     Logs
     on Users.ip = Logs.ip
GROUP BY browser;
