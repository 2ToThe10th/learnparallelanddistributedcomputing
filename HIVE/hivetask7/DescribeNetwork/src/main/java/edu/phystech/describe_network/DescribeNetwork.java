package edu.phystech.describe_network;

import org.apache.hadoop.hive.ql.exec.UDFArgumentException;
import org.apache.hadoop.hive.ql.metadata.HiveException;
import org.apache.hadoop.hive.ql.udf.generic.GenericUDTF;
import org.apache.hadoop.hive.serde2.objectinspector.*;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.PrimitiveObjectInspectorFactory;
import org.apache.hadoop.hive.serde2.objectinspector.primitive.StringObjectInspector;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DescribeNetwork extends GenericUDTF {

    private StringObjectInspector ipObjectInspector;
    private StringObjectInspector maskObjectInspector;

    private Object[] forwardObjArray = new Object[2];

    private static final Pattern ipAddressPattern = Pattern.compile("(\\d+)\\.(\\d+)\\.(\\d+)\\.(\\d+)");
    long allOneIpAddress = (((long) 1) << 32) - 1; //  11111111111111111111111111111111

    @Override
    public StructObjectInspector initialize(StructObjectInspector argOIs) throws UDFArgumentException {
        List<? extends StructField> arguments = argOIs.getAllStructFieldRefs();
        if (arguments.size() != 2) {
            throw new UDFArgumentException(getClass().getSimpleName() + " takes 2 arguments!");
        }

        ipObjectInspector = (StringObjectInspector) arguments.get(0).getFieldObjectInspector();
        maskObjectInspector = (StringObjectInspector) arguments.get(1).getFieldObjectInspector();

        final List<String> fieldNames = new ArrayList<String>();
        fieldNames.add("IP");
        fieldNames.add("IP_number");
        final List<ObjectInspector> fieldInspectors = new ArrayList<ObjectInspector>();
        fieldInspectors.add(PrimitiveObjectInspectorFactory.javaStringObjectInspector);
        fieldInspectors.add(PrimitiveObjectInspectorFactory.javaLongObjectInspector);
        return ObjectInspectorFactory.getStandardStructObjectInspector(fieldNames, fieldInspectors);
    }

    @Override
    public void process(Object[] objects) throws HiveException {
        final String ip = ipObjectInspector.getPrimitiveJavaObject(objects[0]);
        final String mask = maskObjectInspector.getPrimitiveJavaObject(objects[1]);

        long ipNumber = getIPAddressInLong(ip);
        long maskNumber = getIPAddressInLong(mask);
        long ipNetworkStartAddress = ipNumber & maskNumber;
        long ipNetworkToAddress = ipNetworkStartAddress + ((maskNumber - 1) ^ allOneIpAddress);


        for (long ipInNetwork = ipNetworkStartAddress; ipInNetwork < ipNetworkToAddress; ++ipInNetwork) {
            forwardObjArray[0] = getIPAddressInString(ipInNetwork);
            forwardObjArray[1] = ipInNetwork;
            forward(forwardObjArray);
        }
    }

    private long getIPAddressInLong(String ipAddressInput) {
        Matcher matcher = ipAddressPattern.matcher(ipAddressInput);
        if (!matcher.matches()) {
            return 0;
        }
        long multiplyTo = 1;
        long ipAddress = 0;
        for (int i = 4; i > 0; --i) {
            ipAddress += Long.parseLong(matcher.group(i)) * multiplyTo;
            multiplyTo <<= 8;
        }
        return ipAddress;
    }

    private String getIPAddressInString(long ipAddress) {
        StringBuilder ipAddressBuilder = new StringBuilder();
        long mask = ((((long) 1) << 8) - 1) << 24; //  11111111000000000000000000000000
        System.out.println(mask);
        for (int i = 0; i < 4; ++i) {
            ipAddressBuilder.append((ipAddress & mask) >> (8 * (3 - i)));
            if (i != 3) {
                ipAddressBuilder.append('.');
            }
            mask >>= 8;
        }
        return ipAddressBuilder.toString();
    }

    @Override
    public void close() throws HiveException {
    }
}
