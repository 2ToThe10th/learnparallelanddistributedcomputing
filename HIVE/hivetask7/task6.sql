ADD JAR DescribeNetwork/target/DescribeNetwork-1.0.jar;

USE gribal;

CREATE TEMPORARY FUNCTION describe_network as 'edu.phystech.describe_network.DescribeNetwork';

SELECT describe_network(ip, mask)
FROM Subnets
LIMIT 100;
