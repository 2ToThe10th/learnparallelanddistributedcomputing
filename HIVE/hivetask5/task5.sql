ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;

USE gribal;

SELECT TRANSFORM (ip,
                  request_date,
                  http_query,
                  answer_length,
                  http_status,
                  user_info)
    USING
                  'sed "s/http/ftp/g"' AS ip, request_date, http_query, answer_length, http_status, user_info
FROM Logs
LIMIT 10;