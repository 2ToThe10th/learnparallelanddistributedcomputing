ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;

USE gribal;

SELECT request_date, count(distinct http_status) as unique_http_status_count
FROM Logs
GROUP BY request_date
ORDER BY unique_http_status_count DESC;