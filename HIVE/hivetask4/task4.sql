ADD JAR ToMegabyte/target/ToMegabyte-1.0.jar;

USE gribal;

CREATE TEMPORARY FUNCTION to_megabyte as 'edu.phystech.to_megabyte.ToMegabyte';

SELECT to_megabyte(answer_length)
FROM Logs
LIMIT 10;
