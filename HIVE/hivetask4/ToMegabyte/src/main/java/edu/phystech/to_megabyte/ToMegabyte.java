package edu.phystech.to_megabyte;

import org.apache.hadoop.hive.ql.exec.UDF;

public class ToMegabyte extends UDF {
    public int evaluate(int sizeInKilobyte) {
        return sizeInKilobyte / 1024;
    }
}
