ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-contrib.jar;
ADD JAR /opt/cloudera/parcels/CDH/lib/hive/lib/hive-serde.jar;

USE gribal;

DROP TABLE IF EXISTS LogsNotPart;
CREATE EXTERNAL TABLE LogsNotPart
(
    ip            STRING,
    request_date  INT,
    http_query    STRING,
    answer_length SMALLINT,
    http_status   SMALLINT,
    user_info     STRING
)
    ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
        WITH SERDEPROPERTIES (
        "input.regex" = '^([0-9\\.]+)\t{3}(\\d{8})\\d+\t(\\S+)\t(\\d+)\t(\\d+)\t(.*)$'
        )
    STORED AS TEXTFILE
    LOCATION '/data/user_logs/user_logs_M';

SET hive.exec.dynamic.partition.mode=nonstrict;
SET hive.exec.max.dynamic.partitions=1800;
SET hive.exec.max.dynamic.partitions.pernode=200;

DROP TABLE IF EXISTS Logs;
CREATE EXTERNAL TABLE Logs
(
    ip            STRING,
    http_query    STRING,
    answer_length SMALLINT,
    http_status   SMALLINT,
    user_info     STRING
)
    PARTITIONED BY (request_date INT)
    STORED AS PARQUET;

INSERT OVERWRITE TABLE Logs PARTITION (request_date)
SELECT ip, http_query, answer_length, http_status, user_info, request_date
FROM LogsNotPart;

SELECT *
FROM Logs
LIMIT 10;


DROP TABLE IF EXISTS Users;
CREATE EXTERNAL TABLE Users
(
    ip      STRING,
    browser STRING,
    sex     STRING,
    age     TINYINT
)
    ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
        WITH SERDEPROPERTIES (
        "input.regex" = '^([0-9\\.]+)\t(\\S+)\t(\\S+)\t(\\d+)$'
        )
    STORED AS TEXTFILE
    LOCATION '/data/user_logs/user_data_M';

SELECT *
FROM Users
LIMIT 10;


DROP TABLE IF EXISTS IPRegions;
CREATE EXTERNAL TABLE IPRegions
(
    ip     STRING,
    region STRING
)
    ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
        WITH SERDEPROPERTIES (
        "input.regex" = '^([0-9\\.]+)\t(.+)$'
        )
    STORED AS TEXTFILE
    LOCATION '/data/user_logs/ip_data_M';

SELECT *
FROM IPRegions
LIMIT 10;


DROP TABLE IF EXISTS Subnets;
CREATE EXTERNAL TABLE Subnets
(
    ip   STRING,
    mask STRING
)
    ROW FORMAT SERDE 'org.apache.hadoop.hive.serde2.RegexSerDe'
        WITH SERDEPROPERTIES (
        "input.regex" = '^([0-9\\.]+)\t([0-9\\.]+)$'
        )
    STORED AS TEXTFILE
    LOCATION '/data/subnets/variant2';

SELECT *
FROM Subnets
LIMIT 10;